package com.cx.mvc.practise.mapping;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cx.mvc.practise.action.MyAction;

public class AcitonMapping {
	
	public static Map<String, String> getMapping()
	{
		Map<String, String> mapping = new HashMap<String, String>();
		mapping.put("com.cx.mvc.practise.vo.impl.LoginFormBean", "com.cx.mvc.practise.action.impl.LoginAction");
		mapping.put("com.cx.mvc.practise.vo.impl.RegisterFormBean", "com.cx.mvc.practise.action.impl.RegisterAction");
		return mapping;
	}
	
	/**
	 * 得到业务处理action
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static MyAction getFormBean(HttpServletRequest request) throws Exception
	{
		String actionClassPath = getMapping().get(request.getParameter("formBeanName"));
		Class<?> clas = Class.forName(actionClassPath);
		return (MyAction) clas.newInstance();
	}
}
