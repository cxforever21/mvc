package com.cx.mvc.practise.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cx.mvc.practise.vo.FormBean;

public interface MyAction {
	
	String FAIL = "fail";
	
	String SUCCESS = "success";
	
	//return url
	public String execute(HttpServletRequest request, HttpServletResponse response, FormBean formBean, Map<String, String> forwards);
	
	//return result success or fail String
	public String execute(HttpServletRequest request, HttpServletResponse response, FormBean formBean);
}
