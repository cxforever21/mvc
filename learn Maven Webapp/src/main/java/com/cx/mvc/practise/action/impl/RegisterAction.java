package com.cx.mvc.practise.action.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cx.mvc.practise.action.MyAction;
import com.cx.mvc.practise.vo.FormBean;
import com.cx.mvc.practise.vo.impl.RegisterFormBean;

public class RegisterAction implements MyAction {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response, FormBean formBean,
			Map<String, String> forwards) {

		RegisterFormBean registerFormBean = (RegisterFormBean) formBean;
		System.out.println(registerFormBean);
		//业务处理
		String result = "fail";
		request.setAttribute("msg", "该信息来自RegisterAction");
		return forwards.get(result);
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, FormBean formBean) {
		RegisterFormBean registerFormBean = (RegisterFormBean) formBean;
		System.out.println(registerFormBean);
		request.setAttribute("msg", "该信息来自RegisterAction");
		return FAIL;
	}

}
