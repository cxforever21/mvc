package com.cx.mvc.practise.listener;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.cx.mvc.practise.cache.ConfigCache;
import com.cx.mvc.practise.parse.XmlParser;
import com.cx.mvc.practise.parse.vo.XmlBean;

public class MyListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext context = sce.getServletContext();
		String myStrutsPath = context.getInitParameter("my-struts");
		String tomcatPath = context.getRealPath("\\");
		//加载xml配置放进缓存
		Map<String, XmlBean> myStrutsMap = XmlParser.parseMyStrutsXml(tomcatPath + myStrutsPath);
		ConfigCache.addMyStrutsConfig(myStrutsMap);
		System.out.println("加载MyStruts文件完成....");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

}
