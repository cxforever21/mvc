package com.cx.mvc.practise.parse.vo;

import java.util.Map;

public class XmlActionBean {
	
	private String name;
	
	private String className;
	
	private String path;
	
	private Map<String, String> forword;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Map<String, String> getForword() {
		return forword;
	}

	public void setForword(Map<String, String> forword) {
		this.forword = forword;
	}

	@Override
	public String toString() {
		return "XmlActionBean [name=" + name + ", className=" + className
				+ ", path=" + path + ", forword=" + forword + "]";
	}
	
}
