package com.cx.mvc.practise.vo.impl;

import com.cx.mvc.practise.vo.FormBean;

/**
 * 
 * @author Administrator
 *
 */
public class LoginFormBean implements FormBean {
	
	private String name;
	
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginFormBean [name=" + name + ", password=" + password + "]";
	}
	
}
