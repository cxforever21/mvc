package com.cx.mvc.practise.cache;

import java.util.HashMap;
import java.util.Map;

import com.cx.mvc.practise.parse.vo.XmlBean;

public class ConfigCache {
	
	private static Map<String, XmlBean> myStrutsMap = new HashMap<String, XmlBean>();
	
	public static void addMyStrutsConfig(Map<String, XmlBean> map)
	{
		myStrutsMap.putAll(map);
	}
	
	public static Map<String, XmlBean> getMyStrutsMap()
	{
		return myStrutsMap;
	}
}
