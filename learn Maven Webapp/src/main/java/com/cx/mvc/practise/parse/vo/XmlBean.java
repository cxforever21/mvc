package com.cx.mvc.practise.parse.vo;


public class XmlBean {
	
	private XmlFormBean xmlFormBean;
	
	private XmlActionBean xmlActionBean;

	public XmlFormBean getXmlFormBean() {
		return xmlFormBean;
	}

	public void setXmlFormBean(XmlFormBean xmlFormBean) {
		this.xmlFormBean = xmlFormBean;
	}

	public XmlActionBean getXmlActionBean() {
		return xmlActionBean;
	}

	public void setXmlActionBean(XmlActionBean xmlActionBean) {
		this.xmlActionBean = xmlActionBean;
	}

	@Override
	public String toString() {
		return "XmlBean [xmlFormBean=" + xmlFormBean + ", xmlActionBean="
				+ xmlActionBean + "]";
	}
	
}
