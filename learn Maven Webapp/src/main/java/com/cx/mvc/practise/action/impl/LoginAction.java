package com.cx.mvc.practise.action.impl;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cx.mvc.practise.action.MyAction;
import com.cx.mvc.practise.vo.FormBean;
import com.cx.mvc.practise.vo.impl.LoginFormBean;

public class LoginAction implements MyAction {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response, FormBean formBean,
			Map<String, String> forwards) {
		LoginFormBean loginFormBean = (LoginFormBean) formBean;
		
		String result = null;
		//业务处理
		if(loginFormBean.getName().equals("aaa") && loginFormBean.getPassword().equals("aaa"))
		{
			result = "success";
		}
		else
		{
			result = "fail";
		}
		request.setAttribute("msg", "该信息来自LoginAction");
		return forwards.get(result);
	}

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response, FormBean formBean) {
		LoginFormBean loginFormBean = (LoginFormBean) formBean;
		request.setAttribute("msg", "该信息来自LoginAction");
		if(loginFormBean.getName().equals("aaa") && loginFormBean.getPassword().equals("aaa"))
			return SUCCESS;
		return FAIL;
	}

}
