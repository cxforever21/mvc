package com.cx.mvc.practise.reflect;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import com.cx.mvc.practise.action.MyAction;
import com.cx.mvc.practise.cache.ConfigCache;
import com.cx.mvc.practise.parse.vo.XmlBean;
import com.cx.mvc.practise.vo.FormBean;

public class MyReflect {
	
	/**
	 * 封装FormBean
	 * @param request
	 * @return
	 */
	public static FormBean assemblingBeanTest(HttpServletRequest request)
	{
		String formBeanClassPath = request.getParameter("formBeanName");
		return reflactFormBean(request, formBeanClassPath);
	}
	
	public static FormBean reflactFormBean(HttpServletRequest request,
			String formBeanClassPath) {
		FormBean formbean = null;
		try {
			Class<?> clas = Class.forName(formBeanClassPath);
			formbean = (FormBean) clas.newInstance();
			Field[] fields = clas.getDeclaredFields();
			for(Field field : fields)
			{
				field.setAccessible(true);
				field.set(formbean, request.getParameter(field.getName())); 
				field.setAccessible(false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return formbean;
	}
	
	public static MyAction getBusinessAction(String actionClassPath)
	{
		MyAction myAction = null;
		Class<?> actionClass;
		try {
			actionClass = Class.forName(actionClassPath.trim());
			myAction = (MyAction) actionClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return myAction;
	}
}
